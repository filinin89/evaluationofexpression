package com.company;

import java.util.ArrayList;

public class Main {



    public static void main(String[] args) {

        String expression = "(6.7 + 10 - 4)/(1+1*2)+1"; // временно здесь
        boolean check;
        ArrayList<String> polishString;
        double result;

        // проверка входного выражения
        InputChecker inputChecker = new InputChecker();
        check = inputChecker.checkInputString(expression); // после этого метода мы уверены, что выражение только в нужном нам формате, можем отдавать его на разбор
        if(!check) return;

        // перевод выражения в польскую нотацию
        Parser parser = new Parser();
        polishString = parser.convertToPolishNotation(expression);
        System.out.println("Выражение в польской записи " + polishString);

        // подсчет выражения в польской нотации
        Calculator calculator = new Calculator();
        result = calculator.calculate(polishString);
        System.out.println("Результат " + result);


    }
}
