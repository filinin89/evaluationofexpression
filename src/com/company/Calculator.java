package com.company;

import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    private String arithmeticOperators = "+-*/";


    public double calculate(ArrayList<String> input){

        Stack<Double> stack = new Stack<>(); // используем стек переменных типов Double, ведь в нем будут только числа

        for (String x : input) {

            /*if (!arithmeticOperators.contains(x)) // число можно и здесь добавить
            {
                stack.push(Double.valueOf(x));
            }
*/
            switch(x){
                case "+":{
                    stack.push(stack.pop() + stack.pop());
                    break;
                }
                case "-":{
                    Double b = stack.pop(), a = stack.pop();
                    stack.push(a - b);                 // в случае с вычитанием нужно вычесть не из верхнего элемента, поэтому поменяем местами
                    break;
                }
                case "*":{
                    stack.push(stack.pop() * stack.pop()); // перемножаем двапоследних
                    break;
                }
                case "/":{
                    Double b = stack.pop(), a = stack.pop();
                    stack.push(a / b);
                    break;
                }

                default: stack.push(Double.valueOf(x)); // если не оператор, то самое время добавить преобразованное число в стек

            }
        }


        return stack.pop();
    }


}
